const Note = require('../models/Note');

module.exports = {
    getNotes: async (req, res) => {
        const notes = await Note.find({username: req.user.id}).sort({
            date: 'desc'
        }).lean();
        res.render('notes/all-notes', {
            notes: notes
        });
    },

    addNote: (req, res) => {
        res.render('notes/add-note');
    },

    newNote: async (req, res) => {
        const title = req.body.title;
        const description = req.body.description;

        const errors = [];

        if (!title) {
            errors.push({
                text: 'Please enter a title'
            });
        }
        if (!description) {
            errors.push({
                text: 'Please enter a description'
            });
        }

        if (errors.length > 0) {
            res.render('notes/add-note', {
                errors: errors,
                title: title,
                description: description
            });
        } else {
            const newNote = new Note({
                title,
                description
            });
            newNote.username = req.user.id;
            await newNote.save();
            req.flash('success_msg', 'Note saved successfully');
            res.redirect('/notes');
        }
    },

    editNote: async (req, res) => {
        const id = req.params.id;
        const note = await Note.find({
            _id: id
        }).lean();
        res.render('notes/edit-note', {
            note
        });
    },

    updateNote: async (req, res) => {
        const title = req.body.title;
        const description = req.body.description;

        await Note.findByIdAndUpdate(req.params.id, {
            title,
            description
        });
        req.flash('success_msg', 'Note updated successfully');
        res.redirect('/notes');
    },
    
    deleteNote: async (req, res) => {
        await Note.findByIdAndDelete(req.params.id);
        req.flash('success_msg', 'Note deleted successfully');
        res.redirect('/notes');
    }
};