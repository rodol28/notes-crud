const User = require('../models/User');
const passport = require('passport');

module.exports = {
    signup: (req, res) => {
        res.render('users/signup');
    },

    register: async (req, res) => {
        const {
            username,
            email,
            pass,
            confirmpass
        } = req.body;
        const errors = [];
        if (username.length <= 0) {
            errors.push({
                text: 'Please insert your username.'
            })
        }
        if (pass != confirmpass) {
            errors.push({
                text: 'Passwords do not match.'
            });
        }
        if (pass.length < 4) {
            errors.push({
                text: 'Passwords must be at least 4 characters.'
            });
        }
        if (errors.length > 0) {
            res.render('users/signup', {
                errors: errors,
                username: username,
                email: email,
                pass: pass,
                confirmpass: confirmpass
            });
        } else {
            const user = await User.findOne({
                email: email
            });
            if (user === null) {
                const newUser = new User({
                    username,
                    email,
                    pass
                });
                newUser.pass = await newUser.encryptPassword(pass);
                await newUser.save();
                req.flash('success_msg', 'You are registered.');
                res.redirect('/users/signin');
            } else {
                errors.push({
                    text: 'The email is already in use.'
                });
                res.render('users/signup', {
                    errors: errors,
                    username: username,
                    email: email,
                    pass: pass,
                    confirmpass: confirmpass
                });
            }
        }
    },

    signin: (req, res) => {
        res.render('users/signin');
    },

    enter: passport.authenticate('local', {
        successRedirect: '/notes',
        failureRedirect: '/users/signin',
        failureFlash: true
    }),

    logout: (req, res) => {
        req.logout();
        res.redirect('/');
    }
};