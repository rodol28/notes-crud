const router = require('express').Router();
const control = require('../controllers/ctrl-index');

router.get('/', control.home);
router.get('/about', control.about);

module.exports = router;