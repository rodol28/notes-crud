const router = require('express').Router();
const control = require('../controllers/ctrl-users');

router.get('/users/signin', control.signin);
router.post('/users/signin', control.enter);

router.get('/users/signup', control.signup);
router.post('/users/signup', control.register);

router.get('/users/logout', control.logout);

module.exports = router;