const router = require('express').Router();
const control = require('../controllers/ctrl-notes');
const { isAuthenticated } = require('../helpers/auth');

router.get('/notes', isAuthenticated, control.getNotes);
router.get('/notes/add', isAuthenticated, control.addNote);
router.post('/notes/new-note', isAuthenticated, control.newNote);
router.get('/notes/edit/:id', isAuthenticated, control.editNote);
router.put('/notes/edit-note/:id', isAuthenticated, control.updateNote);
router.delete('/notes/delete-note/:id', isAuthenticated, control.deleteNote);

module.exports = router;